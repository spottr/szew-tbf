# Introduction

Start with EDN like this (dev-datasets/sample1.edn):

```clojure
;; sample1.edn
{:conf/user   "DIO"
 :conf/pass   "KonoD10D4"
 :conf/host   "zawarudo.lan"
 :jobs/check1 {:hostname #fill :conf/host
               :username #fill :conf/user
               :password #fill :conf/pass
               :path     "/checks/1"
               :args     {:seconds 1 :user #fill :conf/user}}
 :jobs/check2 {:hostname #fill :conf/host
               :username #fill :conf/user
               :password #fill :conf/pass
               :path     "/checks/2"
               :args     {:seconds 2}}
 :tasks/seq1  [#fill :jobs/check1 #fill :jobs/check2]
 ;; compound keys and recursive fill
 :mark1 #fill [:jobs/check1 :args :user]
 :attr :path
 :mark2 #fill [:jobs/check2 #fill :attr]}
```

Read it like this:

```clojure
(require '[szew.tbf :as tbf])
(require '[clojure.edn :as edn])
(require '[clojure.java.io :refer [reader]])
(import '[java.io PushbackReader])

(def data
  (edn/read {:readers tbf/default-readers
             :eof     (Object.)
             :default vector}
            (PushbackReader. (reader "dev-datasets/sample1.edn"))))
```

And get `data` to be:

```clojure
{:attr :path
 :mark1 #szew.tbf.ToBeFilled {:ident [:jobs/check1 :args :user]}
 :mark2 #szew.tbf.ToBeFilled
 {:ident [:jobs/check2 #szew.tbf.ToBeFilled {:ident :attr}]}
 :conf/host "zawarudo.lan"
 :conf/pass "KonoD10D4"
 :conf/user "DIO"
 :jobs/check1 {:args {:seconds 1 :user #szew.tbf.ToBeFilled {:ident :conf/user}}
               :hostname #szew.tbf.ToBeFilled {:ident :conf/host}
               :password #szew.tbf.ToBeFilled {:ident :conf/pass}
               :path "/checks/1"
               :username #szew.tbf.ToBeFilled {:ident :conf/user}}
 :jobs/check2 {:args {:seconds 2}
               :hostname #szew.tbf.ToBeFilled {:ident :conf/host}
               :password #szew.tbf.ToBeFilled {:ident :conf/pass}
               :path "/checks/2"
               :username #szew.tbf.ToBeFilled {:ident :conf/user}}
 :tasks/seq1 [#szew.tbf.ToBeFilled {:ident :jobs/check1}
              #szew.tbf.ToBeFilled {:ident :jobs/check2}]}
```

All you want here is `:tasks/seq1`, so you can just fill it out and return:

```clojure
(tbf/fill data :tasks/seq1)
```

Or if you want to propagate something outside of `data` map:

```clojure
(tbf/ex-fill data (get data :tasks/seq1))
```

Both calls return fat result:

```clojure
[{:args {:seconds 1 :user "DIO"}
  :hostname "zawarudo.lan"
  :password "KONOD10D4"
  :path "/checks/1"
  :username "DIO"}
 {:args {:seconds 2}
  :hostname "zawarudo.lan"
  :password "KONOD10D4"
  :path "/checks/2"
  :username "DIO"}]
```

Finally `szew.tbf/fill*` can be used to fill the entire LUT or up to a certain key:

```clojure
(tbf/fill* data :jobs/check1)
```

Will build the structure up to `:jobs/check1` and return that:

```clojure
{:attr :path
 :mark1 #szew.tbf.ToBeFilled {:ident [:jobs/check1 :args :user]}
 :mark2 #szew.tbf.ToBeFilled
 {:ident [:jobs/check2 #szew.tbf.ToBeFilled {:ident :attr}]}
 :conf/host "zawarudo.lan"
 :conf/pass "KonoD10D4"
 :conf/user "DIO"
 :jobs/check1 {:args {:seconds 1 :user "DIO"}
               :hostname "zawarudo.lan"
               :password "KonoD10D4"
               :path "/checks/1"
               :username "DIO"}
 :jobs/check2 {:args {:seconds 2}
               :hostname #szew.tbf.ToBeFilled {:ident :conf/host}
               :password #szew.tbf.ToBeFilled {:ident :conf/pass}
               :path "/checks/2"
               :username #szew.tbf.ToBeFilled {:ident :conf/user}}
 :tasks/seq1 [#szew.tbf.ToBeFilled {:ident :jobs/check1}
              #szew.tbf.ToBeFilled {:ident :jobs/check2}]}
```

And without a key goes full-fill:

```clojure
(tbf/fill* data)
```

will produce:

```clojure
{:attr :path
 :mark1 "DIO"
 :mark2 "/checks/2"
 :conf/host "zawarudo.lan"
 :conf/pass "KonoD10D4"
 :conf/user "DIO"
 :jobs/check1 {:args {:seconds 1 :user "DIO"}
               :hostname "zawarudo.lan"
               :password "KonoD10D4"
               :path "/checks/1"
               :username "DIO"}
 :jobs/check2 {:args {:seconds 2}
               :hostname "zawarudo.lan"
               :password "KonoD10D4"
               :path "/checks/2"
               :username "DIO"}
 :tasks/seq1 [{:args {:seconds 1 :user "DIO"}
               :hostname "zawarudo.lan"
               :password "KonoD10D4"
               :path "/checks/1"
               :username "DIO"}
              {:args {:seconds 2}
               :hostname "zawarudo.lan"
               :password "KonoD10D4"
               :path "/checks/2"
               :username "DIO"}]}
```

Note: keys that cannot be filled are returned as is. `szew.tbf/not-yet-filled`
gives a set of unfilled identifiers. `szew.tbf/filled?` predicate uses that
to give you a boolean answer.
