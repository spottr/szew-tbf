# szew/tbf

It might sound a bit silly, but how about some DRY in your data?

[![szew/tbf](https://clojars.org/szew/tbf/latest-version.svg)](https://clojars.org/szew/tbf)

[API documentation][latest] | [Changelog][changelog]

## Usage

`szew/tbf` (_To Be Filled_) denormalizes your structured data while keeping
the dependency resolution order. Placeholders with identifiers are used until
you need to fill them in using a look-up-table, flat or nested.

See [intro][intro] for 1 minute primer.

Please be aware there are better solutions for configuration as data, like
the awesome [juxt/aero][juxt-aero].

## Credits

Tracks dependencies via minimalistic [`weavejester/dependency`][dep], entire
idea is borrowed from [Integrant][integrant] sets up systems, but cut down
and limited in scope.

It uses built-in `clojure.core/tree-seq` to harvest the references,
`clojure.walk/postwalk` to swap them with data.

## License

Copyright © 2021-2022 Sławek Gwizdowski

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
[http://www.eclipse.org/legal/epl-2.0][epl2].

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at [https://www.gnu.org/software/classpath/license.html][gnuce].

[latest]: http://spottr.bitbucket.io/szew-tbf/latest/
[changelog]: CHANGELOG.md
[intro]: doc/intro.md
[dep]: https://github.com/weavejester/dependency/
[juxt-aero]: https://github.com/juxt/aero
[integrant]: https://github.com/weavejester/integrant/
[epl2]: http://www.eclipse.org/legal/epl-2.0
[gnuce]: https://www.gnu.org/software/classpath/license.html
