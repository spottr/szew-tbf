(ns szew.tbf
  (:require [clojure.walk :refer [postwalk]]
            [weavejester.dependency :as dep]))

;; Low level stuff, tee hee.

(defprotocol FillInLink
  (-link? [l] "Is this a resource link to be filled?")
  (-ident [l] "Get identifier for this resource link."))

(extend-type nil
  FillInLink
  (-link? [_] false)
  (-ident [_] nil))

(extend-type #?(:clj Object :cljs object)
  FillInLink
  (-link? [_] false)
  (-ident [_] nil))

(defrecord ToBeFilled [ident]
  FillInLink
  (-link? [_] true)
  (-ident [_] ident))

(def
  ^{:doc "Map of default readers, contains #fill and #provided."}
  default-readers
  {'fill     ->ToBeFilled
   'provided ->ToBeFilled})

(defn- to-be-filled?
  "Is object o to be provided?"
  [o]
  (try (-link? o)
       (catch #?(:clj Exception :cljs :default) _ false)))

;; UI

(defn not-yet-filled
  "Get a set of ToBeProvided identifiers from object o."
  [o]
  (into #{}
        (comp (filter to-be-filled?) (map -ident))
        (tree-seq coll? seq o)))

(defn- links-to-deps
  "Make all links from object o be dependencies of identifier i within
  dependency graph d. KONO DIO DA!"
  [d i o]
  (if (vector? i)
    (reduce (fn [g l] (dep/depend g i l))
            d
            (into (not-yet-filled i) (not-yet-filled o)))
    (reduce (fn [g l] (dep/depend g i l)) d (not-yet-filled o))))

(defn- fill-one
  "Fill object present in map p under identifier i return it. If not found
  ToBeProvided of identifier i is returned instead.

  If found value is itself another ToBeProvided -- resolves recursively."
  [p i]
  (if (contains? p i)
    (postwalk
     (fn [d]
       (if (to-be-filled? d)
         (let [id (-ident d)
               v  (if (vector? id)
                    (get-in p id d)
                    (get p id d))]
           (if (= v d) v (recur v)))
         d))
     (get p i))
    (->ToBeFilled i)))

(defn fill*
  "Fills provided map p recursively up to identifier i. If identifier
  not given fills all keys."
  ([p i]
   (if (contains? p i)
     (let [deps (reduce-kv links-to-deps (dep/graph) p)
           tds  (conj (dep/transitive-dependencies deps i) i)]
       (loop [tbp p todo (filterv tds (dep/topo-sort deps))]
         (if (< 0 (count todo))
           (let [f (first todo)]
             (if (contains? tbp f)
               (recur (assoc tbp f (fill-one tbp f)) (subvec todo 1))
               (recur tbp (subvec todo 1))))
           tbp)))
     p))
  ([p]
   (reduce fill* p (keys p))))

(defn fill
  "Fills provided map p up to identifier i, returns that object."
  [p i]
  (get (fill* p i) i))

(defn filled?
  "Is given object o free of ToBeProvided links?"
  [o]
  (= 0 (count (not-yet-filled o))))

(defn ex-fill
  "Fills and returns object o using provided map p."
  [p o]
  (let [to-be (not-yet-filled o)]
    (if (= 0 (count to-be))
      o
      (let [p+ (reduce fill* p to-be)]
        (postwalk
         (fn [d] (if (to-be-filled? d)
                   (get p+ (-ident d) d)
                   d))
         o)))))