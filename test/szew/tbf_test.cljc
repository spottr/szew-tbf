(ns szew.tbf-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [szew.tbf :refer [not-yet-filled fill* fill filled? ex-fill
                     ->ToBeFilled]]))

(def sample1
  {:A {:B (->ToBeFilled :B) :C (->ToBeFilled :C) :D (->ToBeFilled :D)}
   :B {:C (->ToBeFilled :C) :D (->ToBeFilled :D)}
   :C {:D (->ToBeFilled :D)}
   :D :done})

(deftest simple-fill-test
  
  (testing "not-yet-filled and filled?"
    (is (= #{:B :C :D} (not-yet-filled sample1)))
    (is (false? (filled? sample1)))
    (is (= #{} (not-yet-filled (fill* sample1))))
    (is (true? (filled? (fill* sample1)))))
  
  (testing "Fill up to a key"
    (is (= :done (fill sample1 :D)))
    (is (true? (filled? (fill sample1 :D))))
    (is (= #{} (not-yet-filled (fill sample1 :D))))
    (is (= {:B {:C {:D :done} :D :done} :C {:D :done} :D :done}
           (fill sample1 :A)))
    (is (true? (filled? (fill sample1 :A))))
    (is (= #{} (not-yet-filled (fill sample1 :A))))))

(def sample2 (dissoc sample1 :C))

(deftest partial-fill-test
  
  (testing "not-yet-filled and filled?"
    (is (= #{:B :C :D} (not-yet-filled sample2)))
    (is (false? (filled? sample2)))
    (is (= #{:C} (not-yet-filled (fill* sample2))))
    (is (false? (filled? (fill* sample2)))))
  
  (testing "Fill up to a key"
    (is (= :done (fill sample2 :D)))
    (is (true? (filled? (fill sample2 :D))))
    (is (= #{} (not-yet-filled (fill sample2 :D))))
    (is (= {:B {:C (->ToBeFilled :C) :D :done}
            :C (->ToBeFilled :C)
            :D :done}
           (fill sample2 :A)))
    (is (false? (filled? (fill sample2 :A))))
    (is (= #{:C} (not-yet-filled (fill sample2 :A))))))

(def sample3 {:C {:D (->ToBeFilled :D)}})

(deftest ex-fill-test
  (testing "filling in stages"
    (is (= #{:D} (not-yet-filled sample3)))
    (is (= #{:B :C :D} (not-yet-filled sample2)))
    (let [x (ex-fill sample3 sample2)]
      (is (= #{:B :D} (not-yet-filled x)))
      (is (= {:B {:C {:D :done} :D :done} :C {:D :done} :D :done}
             (ex-fill x (get x :A)))))))

(def sample4 (assoc sample1
                    :X (->ToBeFilled [:A :D])
                    :Y (->ToBeFilled (->ToBeFilled :key))
                    :key (->ToBeFilled :Z)
                    :Z [:A :D]
                    :_ (->ToBeFilled :not-found)))

(deftest compound-keys-and-fill-in-fills
  (testing "Deep filling and fill-in-fills"
    (is (= #{:B :C :D :Z :key [:A :D] (->ToBeFilled :key) :not-found}
           (not-yet-filled sample4)))
    (is (= :done (fill sample4 :X)))
    (is (= :done (fill sample4 :Y)))
    (is (= [:A :D] (fill sample4 :key)))
    (is (= [:A :D] (fill sample4 :Z)))
    (is (= (->ToBeFilled :not-found) (fill sample4 :_)))))