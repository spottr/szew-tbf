(ns user
  (:require
   [szew.tbf :as tbf]
   [clojure.edn :as edn]
   [clojure.java.io :refer [reader]])
  (:import [java.io PushbackReader]))

(defn r! []
  (edn/read {:readers tbf/default-readers
             :eof     (Object.)
             :default vector}
            (PushbackReader. (reader "dev-datasets/sample1.edn"))))