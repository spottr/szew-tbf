# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Changed
- ...

## [0.1.1] - 2022-07-23

### Added

- Compound keys: vector keys now `get-in` within the map
- Identifiers can contain identifiers, filling is recursive

## [0.1.0] - 2021-03-07
### Added

- Initial release!
- `szew.tbf.ToBeFilled` type
- Auto-created constructors `->ToBeFilled` and `map->ToBeFilled`
- `szew.tbf.default-readers`: `#fill` and `#provided` calling `->ToBeFilled`
- `szew.tbf.not-yet-filled`
- `szew.tbf.fill*`
- `szew.tbf.fill`
- `szew.tbf.filled?`
- `szew.tbf.ex-fill`

[Unreleased]: https://bitbucket.org/spottr/szew-io/branches/compare/master%0D0.1.1#diff
[0.1.1]: https://bitbucket.org/spottr/szew-tbf/branches/compare/0.1.1%0D0.1.0
[0.1.0]: https://bitbucket.org/spottr/szew-tbf/branches/compare/0.1.0%0D601e2a26f5059c9da7f36981447375332352a340#diff
