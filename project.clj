(defproject szew/tbf "0.1.2-SNAPSHOT"

  :description "DRY-er data."

  :url "https://bitbucket.org/spottr/szew-tbf/"

  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}

  :dependencies [[org.clojure/clojure "1.11.0"]
                 [weavejester/dependency "0.2.1"]]

  :profiles {:dev {:plugins [[lein-codox "0.10.7"]]
                   :dependencies [[criterium "0.4.6"]
                                  [orchestra "2021.01.01-1"]
                                  [eftest "0.5.9"]
                                  [hashp "0.2.1"]]
                   :global-vars {*warn-on-reflection* true
                                 *print-namespace-maps* false}
                   :source-paths ["dev"]}}

  :codox {:project {:name "szew/tbf"}
          :doc-files ["doc/intro.md" "CHANGELOG.md"]
          :metadata {:doc/format :markdown}
          :namespaces [#"^szew\..*$"]
          ;:exclude-vars #"(?xi)^(?:|map->.*|->[A-Z].*)$"
          :source-uri
          ~(str "https://bitbucket.org/spottr/szew-tbf/src/"
                "a7d9edb9713fb140d345fea00ca5f35a03217782" ; 0.1.1
                "/{filepath}#{basename}-{line}")}

  :repl-options {:init-ns user})
